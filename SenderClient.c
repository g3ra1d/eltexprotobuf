#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <time.h>
#include "amessage.pb-c.h"

#define PortUDP 9998

char printing = 'n';
int my_sock;
    pthread_t ptid,consoleptid;
size_t socket_init_udp(int *sock,int port){

    if ((*sock=socket(AF_INET,SOCK_DGRAM,0))<0){
        perror("Opening socket");
        return -1;
    }
    struct sockaddr_in server;
    server.sin_family = AF_INET;
    server.sin_port = htons(PortUDP);
    server.sin_addr.s_addr = INADDR_ANY;

    while(bind(*sock,(struct sockaddr *)&server, sizeof(server))<0){
        continue;

    }

return sizeof(server);
};
int getMessageFromUDP(){
     int socket_rcv;
     struct sockaddr_in server; // структуры адреса сервера
     socket_init_udp(&socket_rcv,PortUDP);
        int new_port;
        char buff[64]="";
        int n = recvfrom(socket_rcv,&buff,sizeof(buff),NULL,NULL,NULL);
        close(socket_rcv);
         if (strlen(buff) > 8)
        {
            char temp[20];
            int sock;
            sscanf(buff,"%s %d",temp,&sock);
            return sock;
        }
    return atoi(buff);
}


void error(const char *msg)
{
    perror(msg);
    exit(0);
}

void* console(void *arg){

    while(1){
        char symbol;
        scanf("%c",&symbol);
        switch(symbol){
            case 'q': { exit(0);}
            default: break;
        }
    }
    return NULL;
}
void* thread(void *arg){
    while(1){
        int socket_rcv;
        struct sockaddr_in server; // структуры адреса сервера
        socket_init_udp(&socket_rcv,PortUDP);
        int new_port;
        char buff[64]="";
        int n = recvfrom(socket_rcv,&buff,sizeof(buff),NULL,NULL,NULL);
        close(socket_rcv);
        if (strlen(buff)>8){
            char buffer[30];int sock;
            sscanf(buff,"%s %d",buffer,&sock);
            if (strcmp(buffer,"QueueIsFull")==0){
                printf("Очередь полная. Жду освобождения ...\n");
                printing = 'n';
            usleep(900);
        }

        }else printing = 'y';

        usleep(100);
    }
    return NULL;
}

int main(int argc, char *argv[])
{
    if (argc < 2) {
        printf("Укажите сервер!\n");
        exit(-1);
    }
    srand ( time(NULL) );

        pthread_create(&ptid,NULL,thread,NULL);
        pthread_create(&consoleptid,NULL,console,NULL);
    int  portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;

	char buff[1024];
	portno = getMessageFromUDP();

	my_sock = socket(AF_INET, SOCK_STREAM, 0);
    if (my_sock < 0)
        error("ERROR opening socket");
	server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
	bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr,
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
	serv_addr.sin_port = htons(portno);

	if (connect(my_sock,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
        error("ERROR connecting");
      while(1){
      if (printing != 'n'){
         AMessage msg = AMESSAGE__INIT;
         void *buf;
         unsigned int len;
         msg.len = (int32_t)(rand()%11+10);
         msg.time = (int32_t)(rand()%5+1);
         char stringrand[25];
         int i = 0;
         for (i = 0;i<msg.len;i++) stringrand[i] = (char) (rand()%25+97); stringrand[msg.len] = '\0';
         msg.my_message=stringrand;
         len = amessage__get_packed_size(&msg);
         buf = malloc(len);
         amessage__pack(&msg,buf);
         printf("Отправляю на сервер сообщение(сон = %d) : %s\n",msg.time,msg.my_message);
         send(my_sock,buf,len,0);
         sleep(msg.time);
         free(buf);
        }
      }
     close(my_sock);

     return 0;

}

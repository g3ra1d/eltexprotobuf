#include <stdlib.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h>
#include "Queue.h"
#include "Server.h"
#include "amessage.pb-c.h"

void * ThreadSender(void *arg){

    int newsocket = *((int*)arg);
	while (1)
	{
        if (QueueSize <= QueueMaxSize){
        senderSend = 1;
        uint8_t buf[MAX_READ_PROTOBUF];

        int bytes_recv = read(newsocket,&buf[0],MAX_READ_PROTOBUF);
        if (bytes_recv <= 0) {
            return -1;}
        AMessage *msg;
        msg = amessage__unpack(NULL, bytes_recv, buf);
        printf("Добавлено в очередь (очередь,время,длина) %-2d | %d | %d | %s\n",QueueSize,msg->time,msg->len,msg->my_message);
        pthread_mutex_lock(&mutex2);
        addToQueue((void*)buf,(uint8_t)bytes_recv);
        pthread_mutex_unlock(&mutex2);
        amessage__free_unpacked(msg, NULL);
        }else{
            senderSend = 2;
            sleep(1);
        }

    }
    return 0;
}
void * ThreadReceiver(void *arg){
    int newsocket = *((int*)arg);
	while (1)
	{
        struct protobufData* mess;
        int error_code;
        int error_code_size = sizeof(error_code);
        getsockopt(newsocket, SOL_SOCKET, SO_ERROR, &error_code, &error_code_size);
        if (error_code > 0) return NULL;
        pthread_mutex_lock(&mutex);
        if (QueueSize > 0 ){
            receiveSend = 1;
            mess = getFromQueue();
            int count = write(newsocket,mess->dataStorage,mess->dataLen);
            if (count <= 0) {pthread_mutex_unlock(&mutex); return -1;}
            AMessage *msg;
            msg = amessage__unpack(NULL, mess->dataLen, mess->dataStorage);
            printf("Взято из очереди    (очередь,время,длина) %-2d | %d | %d | %s\n",QueueSize,msg->time,msg->len,msg->my_message);
            if (mess != NULL) free(mess);
            pthread_mutex_unlock(&mutex);
            sleep(msg->time);
            amessage__free_unpacked(msg, NULL);
        } else{
            receiveSend = 2;
            sleep(1);
        }
        pthread_mutex_unlock(&mutex);
    }
    return 0;
}
void* ThreadInitSender(void *arg){
struct sockaddr_in serv_addr,cli_addr;
 while(1){

        int createSocket = socket(AF_INET, SOCK_STREAM, 0);
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_addr.s_addr = INADDR_ANY; // сервер принимает подключения на все IP-адреса
        serv_addr.sin_port = 0;
        if (bind(createSocket, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
              error("ERROR on binding");
              listen(createSocket,5);
        socklen_t len = sizeof(serv_addr);
        if(getsockname(createSocket, (struct sockaddr *)&serv_addr, &len) == -1) {
            perror("getsockname");
            return;
        }
        currentFreePortSenders = ntohs(serv_addr.sin_port);
        int newsocket1 = accept(createSocket,(struct sockaddr *) &cliaddr, &client);
        pthread_t ptid;
        pthread_create(&ptid,NULL,ThreadSender,&newsocket1);
        close(createSocket);
    }
}

void* ThreadInitReceiver(void *arg){
struct sockaddr_in serv_addr,cli_addr;
 while(1){

        int createSocket = socket(AF_INET, SOCK_STREAM, 0);
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_addr.s_addr = INADDR_ANY; // сервер принимает подключения на все IP-адреса
        serv_addr.sin_port = 0;
        if (bind(createSocket, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
              error("ERROR on binding");
              listen(createSocket,5);
        socklen_t len = sizeof(serv_addr);
        if(getsockname(createSocket, (struct sockaddr *)&serv_addr, &len) == -1) {
            perror("getsockname");
            return;
        }
        currentFreePortReceivers = ntohs(serv_addr.sin_port);
        int newsocket1 = accept(createSocket,(struct sockaddr *) &cliaddr, &client);
        pthread_t ptid;
        pthread_create(&ptid,NULL,ThreadReceiver,&newsocket1);
        close(createSocket);
    }
}

int main(int argc, char *argv[])
{
    currentFreePortSenders = 0;
    currentFreePortReceivers = 0;
    pthread_t IniterSender, IniterReceiver;
    pthread_create(&IniterSender,NULL,ThreadInitSender,NULL);
    pthread_create(&IniterReceiver,NULL,ThreadInitReceiver,NULL);
    initServer();
    printf("Сервер начал работу, ожидаю клиентов!\n");
	while (1)
	{

        char symbol;
        scanf("%c",&symbol);
        switch(symbol){
            case 'q': { exit(0);}
            default: break;

    }
    }
     return 0;
}

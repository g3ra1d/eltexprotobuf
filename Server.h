#ifndef SERVER_H_INCLUDED
#define SERVER_H_INCLUDED
#include <sys/socket.h>
#include <pthread.h>
#include <stdlib.h>
#define portSender  7777
#define portReceiver  7778
#define portUDPSender 9998
#define portUDPReceiver 9999
pthread_mutex_t	mutex,mutex2;
int sockfd ;
socklen_t client;
struct sockaddr_in cliaddr;
int newsocket;
struct sockaddr_in serv_addr;
int currentFreePortSenders,currentFreePortReceivers;
pthread_t broadcastSendThread, broadcastRecvThread;
short int receiveSend = 1;
short int senderSend = 1;
size_t socket_init_broadcast(int *sock,int port){

    int BroadcastPermission = 1;
    if ( (*sock = socket(AF_INET,SOCK_DGRAM,0)) < 0){
        perror("Error opening socket");
        return -1;
    }

    if ( setsockopt(*sock, SOL_SOCKET, SO_BROADCAST, (char *)&BroadcastPermission,sizeof(BroadcastPermission)) < 0 ) {
        perror("Error setsockopt to broadcast");
        return -1;
    }

return sizeof(0);
};

size_t send_msg(int sock,struct sockaddr_in addr,char* msg,size_t len){
    size_t send_bytes = sendto (sock,msg, len, 0,(struct sockaddr *)&addr, sizeof (addr));
    if( (int)send_bytes < 0 ){
        perror("Sending\n");
    }
    //printf("To port(%d) Message:%s —- bytes[%d]\n",(int)ntohs(addr.sin_port),msg,send_bytes);
    return send_bytes;
};

void* udp_for_senders(void* arg){
    struct sockaddr_in broadcast_senders;
    broadcast_senders.sin_family = AF_INET;
    broadcast_senders.sin_port = htons(portUDPSender);
    broadcast_senders.sin_addr.s_addr = htonl(INADDR_BROADCAST);
    int sockUDPsedn;
    socket_init_broadcast(&sockUDPsedn,portUDPSender);
    while(1){
        if (senderSend == 1){
            char* myMessage = (char*)malloc(sizeof(char)*30);
            sprintf(myMessage,"%d",currentFreePortSenders);
            send_msg(sockUDPsedn,broadcast_senders,myMessage,strlen(myMessage));
            free(myMessage);
            usleep(100);
        }
        if (senderSend == 2){
            char* myMessage = (char*)malloc(sizeof(char)*30);
            sprintf(myMessage,"%s %d","QueueIsFull",currentFreePortSenders);
            send_msg(sockUDPsedn,broadcast_senders,myMessage,strlen(myMessage));
            free(myMessage);
            usleep(100);
        }
    }
return NULL;
}
void* udp_for_receivers(void* arg){
    struct sockaddr_in broadcast_receivers;
    broadcast_receivers.sin_family = AF_INET;
    broadcast_receivers.sin_port = htons(portUDPReceiver);
    broadcast_receivers.sin_addr.s_addr = htonl(INADDR_BROADCAST);
    int sockUDPreceiver;
    socket_init_broadcast(&sockUDPreceiver,portUDPReceiver);
    while(1){
        if (receiveSend == 1){
            char* myMessage = (char*)malloc(sizeof(char)*30);
            sprintf(myMessage,"%d",currentFreePortReceivers);
            send_msg(sockUDPreceiver,broadcast_receivers,myMessage,strlen(myMessage));
            free(myMessage);
            sleep(1);
        }
        if (receiveSend == 2){
            char* myMessage = (char*)malloc(sizeof(char)*30);
            sprintf(myMessage,"%s %d","QueueIsEmpty",currentFreePortReceivers);
            send_msg(sockUDPreceiver,broadcast_receivers,myMessage,strlen(myMessage));
            free(myMessage);
            sleep(1);
        }
    }
return NULL;
}

void initServer(){
    currentFreePortSenders = 0;
    currentFreePortReceivers = 0;
    pthread_create(&broadcastSendThread,NULL,udp_for_senders,NULL);
    pthread_create(&broadcastRecvThread,NULL,udp_for_receivers,NULL);
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = 0;

}

#endif // SERVER_H_INCLUDED

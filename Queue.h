#ifndef QUEUE_INCLUDED
#define QUEUE_INCLUDED
#include <string.h>
#define QueueMaxSize 50

struct protobufData{
    void* dataStorage;
    unsigned int dataLen;
};

struct Queue {
    struct protobufData data;
    struct Queue* next;
};

int QueueSize = 0;
struct Queue *first=NULL;

void addToQueue(void* buf, unsigned int len){
    struct Queue *Insert = (struct Queue*)malloc(sizeof(struct Queue));
    Insert->next = NULL;
    Insert->data.dataLen = len;
    Insert->data.dataStorage = malloc(len);
    memcpy(Insert->data.dataStorage,buf,len);
    if (first == NULL){
        first = Insert;
        QueueSize++;
        return;
    }
    struct Queue* Last=NULL;
    for (Last = first; Last->next!=NULL;Last=Last->next);
    Last->next = Insert;
    QueueSize++;
    return;
}
struct protobufData* getFromQueue(){
    if (first == NULL) return NULL;
    struct protobufData* returned = (struct protobufData*)malloc(sizeof(struct protobufData));
    returned->dataStorage = malloc(first->data.dataLen);
    memcpy(returned->dataStorage,first->data.dataStorage,first->data.dataLen);
    returned->dataLen = first->data.dataLen;
    struct protobufData* tmp = first;
    first = first->next;
    free(tmp->dataStorage);
    free(tmp);
    QueueSize--;
    return returned;
}
void QueueClear(){
    QueueSize = 0;
    struct Queue *tmp = first;
    while (tmp!=NULL){
        tmp = tmp->next;
        free(first);
    }
}


#endif // QUEUE_INCLUDED

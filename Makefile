all:
	gcc main.c amessage.pb-c.c -o Server -lpthread -lprotobuf-c
	gcc SenderClient.c amessage.pb-c.c -o SenderClient -lpthread -lprotobuf-c
	gcc ReceiverClient.c amessage.pb-c.c -o ReceiverClient -lpthread -lprotobuf-c
clean:
	rm ./SenderClient
	rm ./ReceiverClient
	rm ./Server
